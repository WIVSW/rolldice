const { Given, When, Then, defineParameterType } = require("cucumber");
const sinon = require("sinon");
const chai = require("chai");
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const { expect } = require('chai');

const Game = require('../../src/game');
const readline = require('readline-sync');


let consoleLogStub = sinon.stub(console, 'log');
let questionStub = sinon.stub(readline, "question");
let game;

defineParameterType({name: "bool", regexp: /"([^"]*)"/, transformer(text) {return text.toLowerCase()=="true"}});

Given("Game created", function(){
	game = new Game();
});

When("User enter {string}", function(input) {
	questionStub.returns(input);
});

When("Game started", function() {
	game.start();
	expect(consoleLogStub).to.be.calledWith("Hello! Enter number from 0 to 6:");
});

Then("Game receive {string}", function(expected){
});
